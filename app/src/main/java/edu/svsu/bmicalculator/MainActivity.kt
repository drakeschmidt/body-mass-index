package edu.svsu.bmicalculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnCalculate.setOnClickListener {
            val feetIn = findViewById<EditText>(R.id.inputFeet)
            val inchIn = findViewById<EditText>(R.id.inputInch)
            val weightIn = findViewById<EditText>(R.id.inputWeight)

            val feetToInch = feetIn.text.toString().toDouble() *12
            val inch = inchIn.text.toString().toDouble()
            val totalHeight = feetToInch + inch

            val denomenator = totalHeight * totalHeight

            val weight = weightIn.text.toString().toDouble()

            val numerator = 703 * weight

            val bmi = (numerator / denomenator).toString().toDouble()


            val myIntent = Intent(this, secondBMIActivity::class.java)

            myIntent.putExtra("bmi", bmi.toString())

            startActivity(myIntent)


        }








    }
}
