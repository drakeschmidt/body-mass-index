package edu.svsu.bmicalculator

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second_bmi.*
import java.math.RoundingMode
import java.text.DecimalFormat


class secondBMIActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_bmi)

        val myIntent = Intent()

        val bmi = intent.getStringExtra("bmi")


        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.CEILING


        val finalBmi = df.format((bmi.toString().toDouble()))

        txtYourBmi.text = "your calculated BMI is: "
        txtBmi.text = finalBmi

        if (finalBmi.toDouble() < 18.5) {
            txtUnder.setTextColor(Color.parseColor("#18c1c7"))
        }
        else if (finalBmi.toDouble() < 24.9 ) {
            txtNormal.setTextColor(Color.parseColor("#02c719"))
        }
        else if (finalBmi.toDouble() < 29.9 ) {
            txtOver.setTextColor(Color.parseColor("#edba42"))
        }
        else if (finalBmi.toDouble() < 39.9 ) {
            txtObese.setTextColor(Color.parseColor("#eb7d2f"))
        }
        else if (finalBmi.toDouble() > 39.9 ) {
            txtMorbid.setTextColor(Color.parseColor("#e60e0e"))
        }

        btnGoToThirdAct.setOnClickListener {

            val myIntent = Intent(this, thirdBMIActivity::class.java)

//            myIntent.putExtra("bmi", bmi.toString())

            startActivity(myIntent)
        }

    }
}
